<?php

/**
 * Form callback, settings per webform.
 */
function zurmo_webform_settings_form($form, &$form_state, $node) {
  
  $defaults = zurmo_webform_settings($node->nid);

  // show warning when enabled and no mappings.
  $select = db_select('zurmo_webform_component', 'zwc')
    ->fields('zwc')
    ->condition('nid', $node->nid)
    ->range(0, 1)
    ->execute()->fetchAll();
  if (!count($select) && $defaults['active']) {
    drupal_set_message(t('Sending submissions to Zurmo, but no mappings found. Please add !mapping.',
      array('!mapping' => l(t('field mappings'), 'node/' . $node->nid . '/webform/zurmo-mapping'))), 'warning');
  }

  $form['#node'] = $node;
  
  $form['nid'] = array(
    '#type' => 'value',
    '#value' => $defaults['nid'],
  );
  
  $form['status'] = array(
    '#title' => t('Status'),
    '#type' => 'fieldset',
  );
  
  $form['status']['active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Send webform submissions to Zurmo.'),
    '#default_value' => $defaults['active'],
  );

  $form['sync'] = array(
    '#title' => t('Synchronize settings'),
    '#type' => 'fieldset',
  );
  
  $form['sync']['sync_on_insert'] = array(
    '#type' => 'checkbox',
    '#title' => t('Synchronize to Zurmo on new webform submission.'),
    '#default_value' => $defaults['sync_on_insert'],
  );
  
  $form['sync']['sync_on_update'] = array(
    '#type' => 'checkbox',
    '#title' => t('Synchronize to Zurmo on update of existing webform submission.'),
    '#default_value' => $defaults['sync_on_update'],
  );
  
  $form['sync']['sync_on_delete'] = array(
    '#type' => 'checkbox',
    '#title' => t('Delete submission also in Zurmo on delete of webform submission.'),
    '#default_value' => $defaults['sync_on_delete'],
  );
  
  $form['delay'] = array(
    '#title' => t('Delay synchronization'),
    '#type' => 'fieldset',
  );
  
  $form['delay']['cron_sync'] = array(
    '#type' => 'checkbox',
    '#title' => t('Don\'t sync immediately; wait until cron runs.'),
    '#default_value' => $defaults['cron_sync'],
  );
  
  $form['zurmo'] = array(
    '#title' => t('Zurmo settings'),
    '#type' => 'fieldset',
  );
  
  $states = zurmo_webform_get_zurmo_states_list();
  $form['zurmo']['zurmo_stateid'] = array(
    '#title' => t('Zurmo contact/lead status'),
    '#type' => 'select',
    '#options' => $states,
    '#default_value' => $defaults['zurmo_stateid'],
  );
  
  $form['zurmo']['zurmo_create_task'] = array(
    '#type' => 'checkbox',
    '#title' => t('Create task for every new submission.'),
    '#default_value' => $defaults['zurmo_create_task'],
    '#disabled' => TRUE, // not implemented yet.
  );
  
  // Not all zurmo users have the rights to fetch a user list.
  $zurmo_user = zurmo_webform_get_zurmo_user_list();
  if (count($zurmo_user)) {
    $form['zurmo']['zurmo_ownerid'] = array(
      '#title' => t('Zurmo Owner'),
      '#type' => 'select',
      '#options' => $zurmo_user,
      '#default_value' => $defaults['zurmo_ownerid'],
    );
  }
  
  $form['actions'] = array(
    '#type' => 'actions',
  );
  
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

/**
 * Submit callback for webform settings.
 */
function zurmo_webform_settings_form_submit($form, &$form_state) {
  
  // Exclude unnecessary elements.
  form_state_values_clean($form_state);
  
  $values = $form_state['values'];
  $values += zurmo_webform_settings($form['#node']->nid);
  
  $primary_keys = array();
  if (!empty($values['nid'])) {
    $primary_keys[] = 'nid';
  }
  else {
    $values['nid'] = $form['#node']->nid;
  }
  drupal_write_record('zurmo_webform', $values, $primary_keys);
  
  drupal_set_message(t('The configuration options have been saved.'));
}

/**
 * Menu callback, mapping per webform table.
 */
function zurmo_webform_mapping_callback($node) {

  $select = db_select('webform_component', 'component')
    ->fields('component', array('name', 'form_key', 'cid'))
    ->condition('nid', $node->nid);
  $component_results = $select->execute()->fetchAll();
  $component_options = array();
  foreach ($component_results as $component) {
    $component_options[$component->cid] = check_plain($component->name . ' (' . $component->form_key . ')');
  }
  
  module_load_include('inc', 'zurmo_webform', 'zurmo_webform.zurmo_fields');  
  $zurmo_options = array();
  $zurmo_webform_available_fields = zurmo_webform_available_fields();
  foreach ($zurmo_webform_available_fields as $key => $value) {
    $zurmo_options[$key] = check_plain($value['label'] . ' (' . $key . ')');
  }
  
  $select = db_select('zurmo_webform_component', 'zwc')
    ->fields('zwc')
    ->condition('nid', $node->nid)
    ->execute()->fetchAll();
  $rows = array();
  foreach($select as $mapping) {
    $row = array(
      $component_options[$mapping->cid],
      $zurmo_options[$mapping->zurmo_key],
      l(t('delete'), 'node/' . $node->nid . '/webform/zurmo-mapping/delete/' . $mapping->sid),
    );
    $rows[] = $row;
  }

  
  $render_array = array();
  $render_array['mappings'] = array(
    '#theme' => 'table',
    '#header' => array('Webform Field', 'Zurmo Field', 'Operations'),
    '#rows' => $rows,
    '#empty' => t('No field mappings.'),
  );

  return $render_array;
}

/**
 * Form callback, add mapping for webform.
 */
function zurmo_webform_component_add_form($form, &$form_state, $node) {
    
  $select = db_select('webform_component', 'component')
    ->fields('component', array('name', 'form_key', 'cid'))
    ->condition('nid', $node->nid);
  $component_results = $select->execute()->fetchAll();
  $component_options = array();
  foreach ($component_results as $component) {
    $component_options[$component->cid] = check_plain($component->name . ' (' . $component->form_key . ')');
  }
  
  module_load_include('inc', 'zurmo_webform', 'zurmo_webform.zurmo_fields');  
  $zurmo_options = array();
  $zurmo_webform_available_fields = zurmo_webform_available_fields();
  foreach ($zurmo_webform_available_fields as $key => $value) {
    $zurmo_options[$key] = check_plain($value['label']   . ' (' . $key . ')');
  }
  
  $form['nid'] = array(
    '#type' => 'value',
    '#value' => $node->nid,
  );
  
  $form['cid'] = array(
    '#type' => 'select',
    '#title' => t('Webform Field'),
    '#options' => $component_options,
    '#required' => TRUE,
    '#description' => 'Field in Webform to map from',
  );
  
  $form['zurmo_key'] = array(
    '#type' => 'select',
    '#title' => t('Zurmo Field'),
    '#options' => $zurmo_options,
    '#required' => TRUE,
    '#description' => 'Field to map to in Zurmo',
  );
  
  $form['actions'] = array(
    '#type' => 'actions',
  );
  
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  
  return $form;
}

/**
 * Submit callback, add mapping.
 */
function zurmo_webform_component_add_form_submit($form, &$form_state) {
  // Exclude unnecessary elements.
  form_state_values_clean($form_state);
  
  $values = $form_state['values'];

  drupal_write_record('zurmo_webform_component', $values);
  
  drupal_set_message(t('The mapping has been saved.'));
  
  $form_state['redirect'] = 'node/' . $form_state['values']['nid'] . '/webform/zurmo-mapping';
}

/**
 * Menu callback, delete mapping.
 */
function zurmo_webform_component_delete_callback($node, $sid) {
  try {
    db_delete('zurmo_webform_component')
      ->condition('sid', $sid)
      ->condition('nid', $node->nid)
      ->execute();
    drupal_set_message(t('The mapping has been deleted.'));
  } catch (Exception $e) {
    drupal_set_message(t('Error trying to delete mapping in database'), 'error');
  }
  drupal_goto('node/' . $node->nid . '/webform/zurmo-mapping');  
}