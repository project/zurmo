<?php

/**
 * @file
 * Predefined Array of available field mappings for zurmo_webform
 */

function zurmo_webform_available_fields() {
  return array (
    // names
    'title' => array(
      'label' => 'Salutation',
      'structure' => array('title', 'value'),
    ),
    'title_id' => array(
      'label' => 'Salutation ID',
      'structure' => array('title', 'id'),
    ),
    'firstname' => array(
      'label' => 'First Name',
      'structure' => array('firstName'),
    ),
    'lastname' => array(
      'label' => 'Last Name',
      'structure' => array('lastName'),
    ),
    'department' => array(
      'label' => 'Department',
      'structure' => array('department'),
    ),   
    'jobtitle' => array(
      'label' => 'Job Title',
      'structure' => array('jobTitle'),
    ),
    'companyname' => array(
      'label' => 'Company Name',
      'structure' => array('companyName'),
    ),
    'description' => array(
      'label' => 'Description',
      'structure' => array('description'),
    ),
    'website' => array(
      'label' => 'Website',
      'structure' => array('website'),
    ),
  
    // email addresses
    'primaryemail' => array(
      'label' => 'Primary Email',
      'structure' => array('primaryEmail', 'emailAddress'),
    ),
    'primaryemail_optout' => array(
      'label' => 'Primary Email Opt Out',
      'structure' => array('primaryEmail', 'optOut'),
    ),
    'secondaryemail' => array(
      'label' => 'Secondary Email',
      'structure' => array('secondaryEmail', 'emailAddress'),
    ),
    'secondaryemail_optout' => array(
      'label' => 'Secondary Email Opt Out',
      'structure' => array('secondaryEmail', 'optOut'),
    ),
    
    // lead info for ZurmoCRM
    'source' => array(
      'label' => 'Source',
      'structure' => array('source', 'value'),
    ),
    'source_id' => array(
      'label' => 'Source ID',
      'structure' => array('source', 'id'),
    ),
    'industry' => array(
      'label' => 'Industry',
      'structure' => array('industry', 'value'),
    ),
    'industry_id' => array(
      'label' => 'Industry ID',
      'structure' => array('industry', 'id'),
    ),
    'account' => array(
      'label' => 'Account',
      'structure' => array('account', 'id'),
    ),
    
    // phone numbers
    'mobilephone' => array(
      'label' => 'Mobile Phone',
      'structure' => array('mobilePhone'),
    ),
    'officephone' => array(
      'label' => 'Office Phone',
      'structure' => array('officePhone'),
    ),
    'officefax' => array(
      'label' => 'Fax',
      'structure' => array('officeFax'),
    ),
    'state' => array(
      'label' => 'Status ID',
      'structure' => array('state', 'id'),
    ),
    'owner' => array(
      'label' => 'Owner',
      'structure' => array('owner', 'username'),
    ),
    'owner_id' => array(
      'label' => 'Owner ID',
      'structure' => array('owner', 'id'),
    ),
    
    // adddresses
    'primary_address_city' => array(
      'label' => 'Primary Address City',
      'structure' => array('primaryAddress', 'city'),
    ),
    'primary_address_country' => array(
      'label' => 'Primary Address Country',
      'structure' => array('primaryAddress', 'country'),
    ),
    'primary_address_postalcode' => array(
      'label' => 'Primary Address Postal Code',
      'structure' => array('primaryAddress', 'postalCode'),
    ),
    'primary_address_state' => array(
      'label' => 'Primary Address State',
      'structure' => array('primaryAddress', 'state'),
    ),
    'primary_address_street' => array(
      'label' => 'Primary Address Line 1',
      'structure' => array('primaryAddress', 'street1'),
    ),
    'primary_address_street2' => array(
      'label' => 'Primary Address Line 2',
      'structure' => array('primaryAddress', 'street2'),
    ),
    
    'alt_address_city' => array(
      'label' => 'Alternate Address City',
      'structure' => array('secondaryAddress', 'city'),
    ),
    'alt_address_country' => array(
      'label' => 'Alternate Address Country',
      'structure' => array('secondaryAddress', 'country'),
    ),
    'alt_address_postalcode' => array(
      'label' => 'Alternate Address Postal Code',
      'structure' => array('secondaryAddress', 'postalCode'),
    ),
    'alt_address_state' => array(
      'label' => 'Alternate Address State',
      'structure' => array('secondaryAddress', 'state'),
    ),
    'alt_address_street' => array(
      'label' => 'Alternate Address Line 1',
      'structure' => array('secondaryAddress', 'street1'),
    ),
    'alt_address_street2' => array(
      'label' => 'Alternate Address Line 2',
      'structure' => array('secondaryAddress', 'street2'),
    ),
  );
}

function zurmo_webform_data_to_zurmo_contact_array($webform_submission, $mappings) {
  $data = array();
  $values = array();
  $node = node_load($webform_submission->nid);
  $fields = zurmo_webform_available_fields();
  $options = array(
    'delimiter' => variable_get('webform_csv_delimiter', '\t'),
    'components' => array_keys(webform_component_list($node, 'csv', TRUE)),
    'select_keys' => 0,
    'select_format' => 'separate',
    'range_type' => 'all',
  );
  foreach ($mappings as $mapping) {
    if (!isset($values[$mapping->cid])) {
      $values[$mapping->cid] = '';
      $component = isset($node->webform['components'][$mapping->cid]) ? $node->webform['components'][$mapping->cid] : FALSE;
      if ($component) {
        $raw_data = isset($webform_submission->data[$mapping->cid]['value']) ? $webform_submission->data[$mapping->cid]['value'] : NULL;
        if (webform_component_feature($component['type'], 'csv')) {
          $values[$mapping->cid] = webform_component_invoke($component['type'], 'csv_data', $component, $options, $raw_data);
          if (is_array($values[$mapping->cid])) {
            $values[$mapping->cid] = implode(' ', $values[$mapping->cid]);
          }
        }
      }
    }
    if (!strlen($values[$mapping->cid])) {
      continue;
    }
    $structure = $fields[$mapping->zurmo_key]['structure'];
    $key = &$data;
    foreach($structure as $field_name) {
      if (!isset($key[$field_name])) {
        $key[$field_name] = array();
      }
      $key = &$key[$field_name];
    }
    if (is_array($key)) {
      $key = $values[$mapping->cid];
    }
    else {
      $key .= ' ' . $values[$mapping->cid];
    }
    unset($key);
  }
  return $data;
}
