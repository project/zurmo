<?php

/**
 * Form definition
 */
function zurmo_settings($form, &$form_state) {

  if (variable_get('zurmo_password', '') != '' && variable_get('zurmo_user', '') != '' && variable_get('zurmo_url', '') != '') {
    $form['test_connection'] = array(
      '#type' => 'submit',
      '#value' => t('Test connection with existing values'),
      '#executes_submit_callback' => TRUE,
      '#submit' => array('zurmo_settings_test_connection_submit_handler'),
    );
  }

  $form['zurmo_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Zurmo index.php URL'),
    '#default_value' => variable_get('zurmo_url', ''),
    '#required' => TRUE,
    '#description' => t('Enter the URL of your Zurmo CRM System (Ex: http://www.example.com/zurmocrm/app/index.php).'),
  );

  $form['zurmo_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Zurmo Login Username'),
    '#default_value' => variable_get('zurmo_user', ''),
    '#required' => TRUE,
    '#description' => t('Enter the Login Username for the Zurmo CRM.'),
  );

  $form['zurmo_password'] = array(
    '#type' => 'password',
    '#title' => t('Zurmo Login Password'),
    '#maxlength' => 64,
    '#required' => TRUE,
    '#description' => t('Enter the Password for the Zurmo CRM.'),
  );

  // We have a password; Change the description and make the password field optional.
  if (variable_get('zurmo_password', '') != '') {
    $form['zurmo_password']['#description'] .= ' ';
    $form['zurmo_password']['#description'] .= t('(Leave this field empty to leave the password unchanged.)');
    $form['zurmo_password']['#required'] = FALSE;
  }

  $form['#submit'][] = 'zurmo_settings_encrypt_password_submit_handler';

  return system_settings_form($form);
}

/**
 * Submit handler
 * 
 * Encrypts the password or removes it from post, when empty.
 */
function zurmo_settings_encrypt_password_submit_handler($form, &$form_state) {
  $values = &$form_state['values'];
  if (isset($values['zurmo_password']) && trim($values['zurmo_password']) != '') {
    $values['zurmo_password'] = _zurmo_encrypt($values['zurmo_password']);
  } elseif (isset($values['zurmo_password']) && trim($values['zurmo_password']) == '') {
    unset($values['zurmo_password']);
  }
}

/**
 * Submit handler
 * 
 * Test if we can connect successfully to zurmo and get a contact state.
 */
function zurmo_settings_test_connection_submit_handler($form, &$form_state) {
  /**
   * General thought is here:
   * When you cannot not get a contact state, something is wrong.
   * 
   * TODO: check if you can always get this list, no matter what your permissions are.
   * TODO: change this endpoint when you can get user info of the current user.
   */
  $data = zurmo_authenticated_api_call('/contacts/contactState/api/list/', 'GET');
  if (is_array($data) && isset($data['currentPage'])) {
    drupal_set_message(t('Successfully connected to Zurmo CRM at url: !link', array('!link' => l(variable_get('zurmo_url'), variable_get('zurmo_url')))));
  } else {
    drupal_set_message(
    t('Could not connect to Zurmo CRM at url: !link. Please check the !watchdog for errors.',
      array(
        '!link' => l(variable_get('zurmo_url'), variable_get('zurmo_url')),
        '!watchdog' => l(t('Recent log messages'), 'admin/reports/dblog'),
      ))
    , 'error');
  }
}
